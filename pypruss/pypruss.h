#pragma once

#include <Python.h>
#include <prussdrv.h>
#include <pruss_intc_mapping.h>
#include <fcntl.h>

static tpruss_intc_initdata pypruss_init_intc_data = {
	.sysevts_enabled = {
		PRU0_PRU1_INTERRUPT, PRU1_PRU0_INTERRUPT, PRU0_ARM_INTERRUPT,
    PRU1_ARM_INTERRUPT, ARM_PRU0_INTERRUPT, ARM_PRU1_INTERRUPT,
    (char)-1
	},
	.sysevt_to_channel_map = {
		{PRU0_PRU1_INTERRUPT, CHANNEL1}, {PRU1_PRU0_INTERRUPT, CHANNEL0},
		{PRU0_ARM_INTERRUPT, CHANNEL2}, {PRU1_ARM_INTERRUPT, CHANNEL3},
		{ARM_PRU0_INTERRUPT, CHANNEL0}, {ARM_PRU1_INTERRUPT, CHANNEL1},
		{-1,-1}
	},
	.channel_to_host_map = {
		{CHANNEL0, PRU0}, {CHANNEL1, PRU1},
		{CHANNEL2, PRU_EVTOUT0}, {CHANNEL3, PRU_EVTOUT1}, {-1,-1}
	},
	.host_enable_bitmask = (
		PRU0_HOSTEN_MASK | PRU1_HOSTEN_MASK |
		PRU_EVTOUT0_HOSTEN_MASK | PRU_EVTOUT1_HOSTEN_MASK
	)
};
